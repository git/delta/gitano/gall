-- test/test-gall.lua
--
-- Git Abstraction layer for Lua - Top layer tests
--
-- Copyright 2012 Daniel Silverstone <dsilvers@digital-scurf.org>
--
-- For Licence terms, see COPYING
--

-- Step one, start coverage

pcall(require, 'luacov')

local ents = {}
ents.util = require 'gall.util'
ents.ll = require 'gall.ll'
ents.object = require 'gall.object'
ents.tree = require 'gall.tree'
ents.commit = require 'gall.commit'
ents.tag = require 'gall.tag'
ents.repository = require 'gall.repository'

local gall = require 'gall'


local testnames = {}

local real_assert = assert
local total_asserts = 0
local function assert(...)
   local retval = real_assert(...)
   total_asserts = total_asserts + 1
   return retval
end

local function add_test(suite, name, value)
   rawset(suite, name, value)
   testnames[#testnames+1] = name
end

local suite = setmetatable({}, {__newindex = add_test})

function suite.test_gall_not_empty()
   assert(next(gall), "Gall module is empty")
end

for _, ent in ipairs {
   "repository", "util", "ll", "tag", "commit", "tree", "object" } do
   suite["test_gall_"..ent.."_is_"..ent] = function()
      assert(gall[ent] == ents[ent], "gall."..ent.." module is not gall."..ent.." entry")
   end
end

function suite.test_that_git2_present_if_wanted()
   local want_git2 = not(os.getenv "GALL_DISABLE_GIT2")
   local have_git2 = pcall(require, "gall.ll.git2")
   if want_git2 then
      if have_git2 then
	 assert(gall.ll.git2)
      end
   else
      assert(not gall.ll.git2)
   end
end

local count_ok = 0
for _, testname in ipairs(testnames) do
--   print("Run: " .. testname)
   local ok, err = xpcall(suite[testname], debug.traceback)
   if not ok then
      print(err)
      print()
   else
      count_ok = count_ok + 1
   end
end

print(tostring(count_ok) .. "/" .. tostring(#testnames) .. " [" .. tostring(total_asserts) .. "] OK")

os.exit(count_ok == #testnames and 0 or 1)
