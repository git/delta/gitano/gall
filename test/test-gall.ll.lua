-- test/test-gall.ll.lua
--
-- Git Abstraction layer for Lua - Low level git interface functions
--
-- Copyright 2012 Daniel Silverstone <dsilvers@digital-scurf.org>
--
-- For Licence terms, see COPYING
--

-- Step one, start coverage

pcall(require, 'luacov')

local gall = require 'gall'

local testnames = {}

local real_assert = assert
local total_asserts = 0
local function assert(...)
   local retval = real_assert(...)
   total_asserts = total_asserts + 1
   return retval
end

local function add_test(suite, name, value)
   rawset(suite, name, value)
   testnames[#testnames+1] = name
end

local suite = setmetatable({}, {__newindex = add_test})

function suite.test_ll_chomp()
   local instr = "foo\n"
   local outstr = gall.ll.chomp(instr)
   assert(outstr == "foo")
end

function suite.test_get_set_git()
   local now_git = gall.ll.get_set_git()
   assert(now_git == gall.ll.get_set_git())
   assert(now_git == "git")
   assert("foobar" == gall.ll.get_set_git("foobar"))
   assert(gall.ll.get_set_git() == "foobar")
   assert(now_git == gall.ll.get_set_git(now_git))
   assert(now_git == gall.ll.get_set_git())
   assert(now_git == "git")
end

function suite.test_rungit()
   local args = {
      repo = ".",
      env = { GIT_EDITOR = "foobarbaz" },
      stdout = gall.ll.chomp,
      "var", "GIT_EDITOR"
   }
   local why, stdout, stderr = gall.ll.rungit(args)
   assert(why == 0)
   assert(stdout == "foobarbaz")
   assert(stderr == nil or stderr == "")
end

function suite.test_rungit_stderr()
   local args = {
      repo = "/DOES_NOT_EXIST",
      stderr = gall.ll.chomp,
      stdout = gall.ll.chomp,
      "log"
   }
   local why, stdout, stderr = gall.ll.rungit(args)
   assert(why ~= 0)
   assert(stdout == nil or stdout == "")
   assert(stderr:find("DOES_NOT_EXIST"))
end

function suite.test_hash_object()
   local args = {
      repo = ".",
      stdin = "FOOBAR",
      "-t", "blob", "--stdin",
   }
   local why, stdout, stderr = gall.ll.hash_object(args)
   assert(why == 0)
   assert(stdout == "389865bb681b358c9b102d79abd8d5f941e96551")
   assert(stderr == nil or stderr == "")
end

local count_ok = 0
for _, testname in ipairs(testnames) do
--   print("Run: " .. testname)
   local ok, err = xpcall(suite[testname], debug.traceback)
   if not ok then
      print(err)
      print()
   else
      count_ok = count_ok + 1
   end
end

print(tostring(count_ok) .. "/" .. tostring(#testnames) .. " [" .. tostring(total_asserts) .. "] OK")

os.exit(count_ok == #testnames and 0 or 1)
