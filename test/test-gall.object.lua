-- test/test-gall.object.lua
--
-- Git Abstraction layer for Lua - Object tests
--
-- Copyright 2012 Daniel Silverstone <dsilvers@digital-scurf.org>
--
-- For Licence terms, see COPYING
--

-- This test includes object wibbling
dofile "test/withrepo.lua"

-- Step one, start coverage

pcall(require, 'luacov')

local gall = require 'gall'

local testnames = {}

local real_assert = assert
local total_asserts = 0
local function assert(...)
   local retval = real_assert(...)
   total_asserts = total_asserts + 1
   return retval
end

local function add_test(suite, name, value)
   rawset(suite, name, value)
   testnames[#testnames+1] = name
end

local suite = setmetatable({}, {__newindex = add_test})

local FOOBAR_SHA1 = "389865bb681b358c9b102d79abd8d5f941e96551"

function suite.create_bad()
   local repo = assert(gall.repository.create "test/test_repo")
   local obj, msg = gall.object.create(repo, "BONGHIT", "FOOBAR")
   assert(not obj)
   assert(msg:find(" returned "))
end

function suite.create_blob()
   local repo = assert(gall.repository.create "test/test_repo")
   local obj, msg = gall.object.create(repo, "blob", "FOOBAR")
   assert(obj, msg)
end

function suite.object_tostring()
   local repo = assert(gall.repository.create "test/test_repo")
   local obj, msg = gall.object.create(repo, "blob", "FOOBAR")
   assert(obj, msg)
   local objstr = tostring(obj)
   assert(objstr:find("GitObject"))
   assert(objstr:find("test/test_repo"))
   assert(objstr:find(FOOBAR_SHA1))
end

function suite.object_type()
   local repo = assert(gall.repository.create "test/test_repo")
   local obj, msg = gall.object.create(repo, "blob", "FOOBAR")
   assert(obj, msg)
   assert(obj.type == "blob")
end

function suite.object_size()
   local repo = assert(gall.repository.create "test/test_repo")
   local obj, msg = gall.object.create(repo, "blob", "FOOBAR")
   assert(obj, msg)
   assert(obj.size == 6)
end

function suite.object_raw()
   local repo = assert(gall.repository.create "test/test_repo")
   local obj, msg = gall.object.create(repo, "blob", "FOOBAR")
   assert(obj, msg)
   assert(obj.raw == "FOOBAR")
end

function suite.object_blob_content()
   local repo = assert(gall.repository.create "test/test_repo")
   local obj, msg = gall.object.create(repo, "blob", "FOOBAR")
   assert(obj, msg)
   assert(obj.content == "FOOBAR")
end

function suite.object_commit_content()
   local repo = test_repo()
   local obj = repo:get(repo.HEAD)
   assert(obj.type == "commit")
   local commit = obj.content
   assert(commit.sha == obj.sha)
end

function suite.object_tree_content()
   local repo = test_repo()
   local obj = repo:get(repo.HEAD)
   assert(obj.type == "commit")
   local commit = obj.content
   local treeobj = commit.tree
   local tree = treeobj.content
   local treestr = tostring(tree)
   assert(treestr:find("GitTree"))
end

function suite.object_tag_content()
   local repo = test_repo()
   local obj = repo:get("refs/tags/v1.0")
   assert(obj.type == "tag")
   local tag = obj.content
   assert(tag.sha == obj.sha)
end

function suite.object_get_crap()
   local repo = test_repo()
   local obj = repo:get("refs/tags/v1.0")
   assert(not xpcall(function() obj.trank() end))
end

local count_ok = 0
for _, testname in ipairs(testnames) do
--   print("Run: " .. testname)
   local ok, err = xpcall(suite[testname], debug.traceback)
   if not ok then
      print(err)
      print()
   else
      count_ok = count_ok + 1
   end
end

print(tostring(count_ok) .. "/" .. tostring(#testnames) .. " [" .. tostring(total_asserts) .. "] OK")

os.exit(count_ok == #testnames and 0 or 1)
