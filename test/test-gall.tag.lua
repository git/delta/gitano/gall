-- test/test-gall.tag.lua
--
-- Git Abstraction layer for Lua - Tag object tests
--
-- Copyright 2012 Daniel Silverstone <dsilvers@digital-scurf.org>
--
-- For Licence terms, see COPYING
--

-- This test includes object wibbling
dofile "test/withrepo.lua"

-- Step one, start coverage

pcall(require, 'luacov')

local gall = require 'gall'

local testnames = {}

local real_assert = assert
local total_asserts = 0
local function assert(...)
   local retval = real_assert(...)
   total_asserts = total_asserts + 1
   return retval
end

local function add_test(suite, name, value)
   rawset(suite, name, value)
   testnames[#testnames+1] = name
end

local suite = setmetatable({}, {__newindex = add_test})

function suite.tag_get()
   local repo = test_repo()
   local tag = repo:get("refs/tags/v1.0").content
   assert(tag)
end

function suite.tag_tostring()
   local repo = test_repo()
   local tag = repo:get("refs/tags/v1.0").content
   local tagstr = tostring(tag)
   assert(tagstr:find("GitTag"))
end

function suite.tag_message()
   local repo = test_repo()
   local tag = repo:get("refs/tags/v1.0").content
   local tagmsg = tag.message
   assert(tagmsg:find("Annotated"))
end

function suite.tag_object()
   local repo = test_repo()
   local tag = repo:get("refs/tags/v1.0").content
   local obj = tag.object
   assert(obj)
   assert(obj.type == "commit")
end

function suite.tag_tagger()
   local repo = test_repo()
   local tag = repo:get("refs/tags/v1.0").content
   local person = tag.tagger
   assert(person.realname:find("Committer"))
   assert(person.email:find("gall%-committer"))
   assert(person.unixtime == "1347114766")
   assert(person.timezone == "+0100")
end

function suite.tag_signature()
   local repo = test_repo()
   local tag = repo:get("refs/tags/signed-tag").content
   local sig = tag.signature
   assert(sig)
end

local count_ok = 0
for _, testname in ipairs(testnames) do
--   print("Run: " .. testname)
   local ok, err = xpcall(suite[testname], debug.traceback)
   if not ok then
      print(err)
      print()
   else
      count_ok = count_ok + 1
   end
end

print(tostring(count_ok) .. "/" .. tostring(#testnames) .. " [" .. tostring(total_asserts) .. "] OK")

os.exit(count_ok == #testnames and 0 or 1)
