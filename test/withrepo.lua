-- test/withrepo.lua
--
-- Gall -- Test utilities for when you have repositories
--

local _xpcall = xpcall

function xpcall(fn, tb)
   os.execute("rm -rf test/test_repo")
   os.execute("rm -rf test/lorries.git")
   os.execute("rm -rf test/website.git")
   local ok, msg = _xpcall(fn, tb)
   os.execute("rm -rf test/test_repo")
   os.execute("rm -rf test/lorries.git")
   os.execute("rm -rf test/website.git")
   return ok, msg
end

function test_repo(reponame)
   reponame = reponame or "test_repo"
   os.execute("sh test/create_test_repo.sh")
   return assert(require("gall.repository").new("test/" .. reponame))
end
