-- gall.lua
--
-- Git Abstration Layer for Lua -- Top level
--
-- Copyright 2012 Daniel Silverstone <dsilvers@digital-scurf.org>
--

---
-- Git Abstraction Layer for Lua
--
-- Gall is an abstraction layer, allowing Lua programs to manipulate Git
-- repositories without needing to run the Git program and process its output
-- directly.  Gall will use libgit2 where it has in-process code to do so, and
-- will call out to the Git binary where reimplementing things in-process would
-- be awkward or error-prone.
--
-- **NOTE**: `gall` is not trying to be a complete API onto Git at this time.
-- Instead it is aiming to provide a fairly complete read-only interface to
-- enough of a Git repository to facilitate a Git server's ACLs and also enough
-- of a write-interface to ensure that such a server can store its config etc
-- in a Git repository.
--
-- @module gall

local API_VERSION = 0
local ABI_VERSION = 0

local VERSION = "Gall v1." .. tostring(API_VERSION)

local util = require "gall.util"
local ll = require "gall.ll"

local repository = require "gall.repository"
local tag = require "gall.tag"
local commit = require "gall.commit"
local tree = require "gall.tree"
local object = require "gall.object"

return {
   _API_VERSION = API_VERSION,
   _ABI_VERSION = ABI_VERSION,
   VERSION = VERSION,

   repository = repository,
   tag = tag,
   commit = commit,
   tree = tree,
   object = object,

   ll = ll,
   util = util,
}
