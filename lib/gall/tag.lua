-- gall.tag
--
-- Git Abstraction Layer for Lua -- Tag object interface
--
-- Copyright 2012 Daniel Silverstone <dsilvers@digital-scurf.org>
--
--

---
-- Tag object interface
--
-- @module gall.tag

local ll = require "gall.ll"

local objs = setmetatable({}, {__mode="k"})
local repos = setmetatable({}, {__mode="k"})
local parsed = setmetatable({}, {__mode="k"})

---
-- Who, when
--
-- Encapsulation of git's author/committer who and when data
--
-- @type whowhen

---
-- The "real" name of the person.
--
-- @field realname

---
-- The email address of the person.
--
-- @field email

---
-- The UNIX time (seconds since epoch) for the signature line
--
-- @field unixtime

---
-- The timezone in which the signature took place (+/-HHHH)
--
-- @field timezone

--- @section end

local function parse_person(pers)
   local real, email, when, tz = pers:match("^(.-) <([^>]+)> ([0-9]+) ([+-][0-9]+)$")
   return {
      realname = real,
      email = email,
      unixtime = when,
      timezone = tz
   }
end

local PGP_SIG_START = "-----BEGIN PGP SIGNATURE-----"

local function tagindex(tag, field)
   if not parsed[tag] then
      local raw = objs[tag].raw
      local sigcert = {}
      local headers, body, signature = {}, ""
      local state = "headers"

      for l in raw:gmatch("([^\n]*)\n") do
	 if state == "headers" then
	    sigcert[#sigcert+1] = l
	    local first, second = l:match("^([^ ]+) (.+)$")
	    if first then
	       assert(not headers[first])
	       headers[first] = { second }
	    else
	       state = "message"
	    end
	 elseif state == "message" then
	    if l == PGP_SIG_START then
	       signature = l .. "\n"
	       state = "signature"
	    else
	       body = body .. l .. "\n"
	    end
	 else
	    signature = signature .. l .. "\n"
	 end
      end
      sigcert[#sigcert+1] = body

      -- there's always one object
      rawset(tag, "object", repos[tag]:get(headers.object[1]))
      -- Always one type
      rawset(tag, "type", headers.type[1])
      -- Always one tag name
      rawset(tag, "tag", headers.tag[1])
      -- Always one tagger
      rawset(tag, "tagger", parse_person(headers.tagger[1]))
      -- A message
      rawset(tag, "message", body)
      -- And an optional signature
      rawset(tag, "signature", signature)
      -- And the certificate it signed
      rawset(tag, "signedcert", table.concat(sigcert, "\n"))
      -- Promote the SHA
      rawset(tag, "sha", objs[tag].sha)

      -- Signal we are parsed
      parsed[tag] = true
   end

   return rawget(tag, field)
end

---
-- Encapsulation of a tag object in a Git repository
--
-- @type tag

---
-- The object which has been tagged
--
-- @field object

---
-- The type of the tagged object
--
-- @field type

---
-- The tag name itself
--
-- @field tag

---
-- The tagger as a @{whowhen} instance.
--
-- @field tagger

---
-- The tagging message
--
-- @field message

---
-- The signature on the tag (if present)
--
-- @field signature

---
-- The certificate which is signed by the signature (if present)
--
-- @field signedcert

---
-- The SHA1 OID of the tag
--
-- @field sha

--- @section end

local function tagtostring(tag)
   return "<GitTag(" .. tostring(objs[tag].sha) .. ") in " .. tostring(repos[tag]) .. ">"
end

local tagmeta = {
   __index = tagindex,
   __tostring = tagtostring
}

---
-- Create a new @{tag} instance to represent a tag object in a repository.
--
-- @function new
-- @tparam repository repo The repository containing the tag object.
-- @tparam object obj The low level git object representing the tag.
-- @treturn tag The tag object representing the tag.

local function _new(repo, obj)
   local ret =  setmetatable({}, tagmeta)
   objs[ret] = obj
   repos[ret] = repo
   return ret
end

return {
   new = _new
}
