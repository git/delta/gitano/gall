-- gall.commit
--
-- Git Abstraction Layer for Lua -- Commit object interface
--
-- Copyright 2012 Daniel Silverstone <dsilvers@digital-scurf.org>
--
--

---
-- Commit object interface
--
-- @module gall.commit

local ll = require "gall.ll"

local objs = setmetatable({}, {__mode="k"})
local repos = setmetatable({}, {__mode="k"})
local parsed = setmetatable({}, {__mode="k"})

local _new

---
-- Who, when
--
-- Encapsulation of git's author/committer who and when data
--
-- @type whowhen

---
-- The "real" name of the person.
--
-- @field realname

---
-- The email address of the person.
--
-- @field email

---
-- The UNIX time (seconds since epoch) for the signature line
--
-- @field unixtime

---
-- The timezone in which the signature took place (+/-HHHH)
--
-- @field timezone

--- @section end

local function parse_person(pers)
   local real, email, when, tz = pers:match("^(.-) <([^>]+)>.-([0-9]+) ([+-][0-9]+)$")
   return {
      realname = real,
      email = email,
      unixtime = when,
      timezone = tz
   }
end

local function parse_parents(repo, ps)
   local ret = {}
   for _, psha in ipairs(ps) do
      ret[_] = repo:get(psha)
   end
   return ret
end

local function commitindex(commit, field)
   if not parsed[commit] then
      local raw = objs[commit].raw
      local sigcert = {}
      local headers, body, signature = {}, ""
      local state = "headers"

      for l in raw:gmatch("([^\n]*)\n") do
	 if state == "headers" then
	    local first, second = l:match("^([^ ]+) (.+)$")
	    if first then
	       if first == "gpgsig" then
		  signature = second .. "\n"
		  state = "signature"
	       else
		  sigcert[#sigcert+1] = l
		  if headers[first] then
		     headers[first][#headers[first]+1] = second
		  else
		     headers[first] = { second }
		  end
	       end
	    else
	       state = "message"
	       sigcert[#sigcert+1] = l
	    end
	 elseif state == "signature" then
	    signature = signature .. l:sub(2) .. "\n"
	    if l:find("END PGP SIG") then
	       state = "headers"
	    end
	 else
	    body = body .. l .. "\n"
	 end
      end
      sigcert[#sigcert+1] = body

      -- there's always one tree
      rawset(commit, "tree", repos[commit]:get(headers.tree[1]))
      -- Always one author
      rawset(commit, "author", parse_person(headers.author[1]))
      -- Always one committer
      rawset(commit, "committer", parse_person(headers.committer[1]))
      -- Zero or more parents
      headers.parent = headers.parent or {}
      rawset(commit, "parents", parse_parents(repos[commit], headers.parent))
      -- A message
      rawset(commit, "message", body)
      -- And an optional signature
      rawset(commit, "signature", signature)
      -- The optional signature has to sign something
      rawset(commit, "signedcert", table.concat(sigcert, "\n"))
      -- Promote the SHA
      rawset(commit, "sha", objs[commit].sha)

      -- Signal we are parsed
      parsed[commit] = true
   end

   return rawget(commit, field)
end

local function committostring(commit)
   return "<GitCommit(" .. tostring(objs[commit].sha) .. ") in " .. tostring(repos[commit]) .. ">"
end

---
-- Commit object.
--
-- @type commit

---
-- The tree object referenced by this commit.
--
-- @field tree
-- @see gall.tree

---
-- The author of the commit as a @{whowhen} object
--
-- @field author

---
-- The committer of the commit as a @{whowhen} object
--
-- @field committer

---
-- The parents of the commit (zero or more @{commit} objects)
--
-- @field parents

---
-- The commit message
--
-- @field message

---
-- The signature on the commit (if present)
--
-- @field signature

---
-- The certificate which the signature (if present) signs
--
-- @field signedcert

---
-- The SHA1 OID of the commit
--
-- @field sha

--- @section end

local commitmeta = {
   __index = commitindex,
   __tostring = committostring
}

---
-- Instantiate a new @{commit} object for the given raw git object
--
-- @function new
-- @tparam repository repo The repository containing the commit
-- @tparam object obj The raw git object for the commit
-- @treturn commit The new commit instance

local function _new(repo, obj)
   local ret =  setmetatable({}, commitmeta)
   objs[ret] = obj
   repos[ret] = repo
   return ret
end

---
-- Create a new commit object in the repository.
--
-- The given data must contain all of:
--
-- * tree - The tree object for the commit
-- * author - The author (as a table of realname and email)
-- * committer - The committer (as a table of realname and email)
-- * message - The commit message
--
-- It may optionally contain:
--
-- * parents - The list of parent commit objects.
--
-- @function create
-- @tparam repository repo The repository to create the commit in
-- @tparam table data The commit data
-- @treturn[1] commit The newly added commit object
-- @treturn[2] nil Nil on error
-- @treturn[2] string The error message

local function _create(repo, data)
   if not data.tree then
      return nil, "No tree?"
   end
   if not data.author then
      return nil, "No author?"
   end
   if not data.committer then
      return nil, "No committer?"
   end
   if not data.author.realname then
      return nil, "No author name?"
   end
   if not data.author.email then
      return nil, "No author email?"
   end
   if not data.committer.realname then
      return nil, "No committer name?"
   end
   if not data.committer.email then
      return nil, "No committer email?"
   end
   if not data.message then
      return nil, "No message?"
   end
   if not data.parents then
      data.parents = {}
   end

   -- Construct the commandline and environment
   local env = {
      GIT_AUTHOR_NAME = data.author.realname,
      GIT_AUTHOR_EMAIL = data.author.email,
      GIT_COMMITTER_NAME = data.committer.realname,
      GIT_COMMITTER_EMAIL = data.committer.email,
   }
   local cmd = {
      "commit-tree", data.tree.sha
   }
   for i, v in ipairs(data.parents) do
      cmd[#cmd+1] = "-p"
      if not v.sha then
	 return nil, "Parent " .. tostring(i) .. " had no sha?"
      end
      cmd[#cmd+1] = v.sha
   end

   if not data.message:match("\n$") then
      data.message = data.message .. "\n"
   end

   local why, sha, err = repo:_run_with_input_and_env(env, data.message,
						      ll.chomp, unpack(cmd))

   if why ~= 0 then
      return nil, "commit-tree returned " .. tostring(why) .. 
	 (sha or "") .. "\n" .. (err or "")
							       
   end

   return repo:get(sha)
end

return {
   create = _create,
   new = _new
}
