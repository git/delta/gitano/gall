all: test

MODULES := gall gall.util gall.ll \
	   gall.repository gall.object gall.commit gall.tag gall.tree
LUA_VER := 5.2

PREFIX ?= /usr/local

INST_BASE := $(PREFIX)
INST_ROOT := $(DESTDIR)$(INST_BASE)/share/lua/$(LUA_VER)
C_INST_ROOT := $(DESTDIR)$(INST_BASE)/lib/lua/$(LUA_VER)

ifeq ($(shell pkg-config --exists libgit2 && echo YAY),YAY)
HAVE_GIT2 := YES
GIT2_CFLAGS := $(shell pkg-config --cflags libgit2)
GIT2_LIBS := $(shell pkg-config --libs libgit2)
else
HAVE_GIT2 := NO
GIT2_CFLAGS :=
GIT2_LIBS :=
endif

INCS := -I/usr/include/lua$(LUA_VER) $(GIT2_CFLAGS)
OPT := -O0 -g
PIC := -fPIC
WARN := -Wall -Werror
DEFS := 
CFLAGS := $(INCS) $(OPT) $(WARN) $(DEFS) $(PIC) $(CFLAGS)
LFLAGS := -O1 -g $(GIT2_LIBS) $(LFLAGS)

MOD_FILES := $(patsubst %,%.lua,$(subst .,/,$(MODULES)))

NCPUS := $(shell getconf _NPROCESSORS_ONLN 2>/dev/null || echo 1)
DASH_J := -j $(shell expr \( $(NCPUS) \* 3 \) / 2)

install: cmodule
	mkdir -p $(INST_ROOT)/gall
	for MOD in $(sort $(MOD_FILES)); do \
		cp lib/$${MOD} $(INST_ROOT)/$${MOD}; \
	done
	if test -e lib/gall/ll/git2.so; then \
		mkdir -p $(C_INST_ROOT)/gall/ll; \
		cp lib/gall/ll/git2.so $(C_INST_ROOT)/gall/ll/; \
	fi


LUA := LUA_CPATH="$(shell pwd)/lib/?.so;;" LUA_PATH="$(shell pwd)/lib/?.lua;$(shell pwd)/extras/luacov/src/?.lua;;" __GALL_LL_GIT2_BLACKLIST="$(shell lua$(LUA_VER) find-git2)" lua$(LUA_VER) -e "dofile('blacklist-system-git2')"

ifeq ($(HAVE_GIT2),YES)
cmodule: lib/gall/ll/git2.so

lib/gall/ll/git2.so: lib/gall/ll/git2.c
	$(CC) $(CFLAGS) lib/gall/ll/git2.c -o $@ -shared $(LFLAGS)

else
cmodule:
	@echo "Unable to compile C module, libgit2 not found. SKIPPING"
endif

clean:
	$(RM) luacov.report.out luacov.report.git2.out luacov.stats.out
	$(RM) -r lib/gall/ll/git2.so

distclean: clean
	find . -name "*~" -delete

.PHONY: test
test: cmodule
	@$(RM) luacov.stats.out; \
	ERR=0; \
	for MOD in $(sort $(MODULES)); do \
		echo -n "$${MOD}: "; \
		$(LUA) test/test-$${MOD}.lua; \
		test "x$$?" = "x0" || ERR=1; \
	done; \
	$(LUA) extras/luacov/src/bin/luacov -X luacov. -X test. $(MODULES); \
	mv luacov.report.out luacov.report.git2.out; \
	for MOD in $(sort $(MODULES)); do \
		echo -n "$${MOD} [no git2]: "; \
		GALL_DISABLE_GIT2=1 $(LUA) test/test-$${MOD}.lua; \
		test "x$$?" = "x0" || ERR=1; \
	done; \
	$(LUA) extras/luacov/src/bin/luacov -X luacov. -X test. $(MODULES); \
	exit $$ERR
ifeq ($(HAVE_GIT2),YES)
	@echo -n "Allowing use of luagit2 saves "; \
	PRE=$$(grep sp.spawn_simple luacov.report.git2.out | awk '{print $$1}'); \
	POST=$$(grep sp.spawn_simple luacov.report.out | awk '{print $$1}'); \
	NORM=$$((POST - PRE)); \
	echo -n $$((NORM - PRE)) / $${NORM}; \
	echo " calls out to git executable during test suite."
endif

.PHONY: interactive
interactive:
	$(LUA) -e'gall=require"gall"' -i
